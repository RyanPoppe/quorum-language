package Libraries.Interface.Layouts

use Libraries.Interface.Item2D
use Libraries.Containers.Array
use Libraries.Interface.Controls.Control
use Libraries.Interface.Controls.Dialog

class ManualLayout is Layout

    boolean layoutContainer = false

    action ApplyToContainer(boolean shouldLayout)
        layoutContainer = shouldLayout
    end

    action IsAppliedToContainer returns boolean
        return layoutContainer
    end

    action Layout(Control container, Array<Item2D> items, number containerWidth, number containerHeight)

        if layoutContainer
            container:ResetLayoutFlag()
            LayoutProperties properties = container:GetLayoutProperties(containerWidth, containerHeight)
            Item2D containerParent = container:GetParent()

            if properties not= undefined
                if properties:NeedsRendering()
                    container:LoadGraphics(properties)
                end

                number width = container:GetWidth()
                number height = container:GetHeight()
                number parentWidth = 0
                number parentHeight = 0
                if containerParent not= undefined
                    parentWidth = containerParent:GetWidth()
                    parentHeight = containerParent:GetHeight()
                end

                if properties:IsSettingWidth()
                    width = parentWidth * properties:GetPercentageWidth() + properties:GetPixelWidth() * properties:GetInterfaceScale()
                end

                if properties:IsSettingHeight()
                    height = parentHeight * properties:GetPercentageHeight() + properties:GetPixelHeight() * properties:GetInterfaceScale()
                end

                if width < properties:GetMinimumWidth()
                    width = properties:GetMinimumWidth()
                elseif width > properties:GetMaximumWidth()
                    width = properties:GetMaximumWidth()
                end

                if height < properties:GetMinimumHeight()
                    height = properties:GetMinimumHeight()
                elseif height > properties:GetMaximumHeight()
                    height = properties:GetMaximumHeight()
                end

                if properties:IsSnappingToPixels()
                    width = cast(integer, width)
                    height = cast(integer, height)
                end

                if properties:IsPositioningInX()
                    if properties:IsPositioningInY()
                        number targetX = parentWidth * properties:GetPercentageX() + properties:GetPixelX() - width * properties:GetPercentageOriginX()
                        number targetY = parentHeight * properties:GetPercentageY() + properties:GetPixelY() - height * properties:GetPercentageOriginY()

                        if properties:IsSnappingToPixels()
                            targetX = cast(integer, targetX)
                            targetY = cast(integer, targetY)
                        end

                        container:SetPosition(targetX, targetY)
                    else
                        number targetX = parentWidth * properties:GetPercentageX() + properties:GetPixelX() - width * properties:GetPercentageOriginX()
                        
                        if properties:IsSnappingToPixels()
                            targetX = cast(integer, targetX)
                        end

                        container:SetX(targetX)
                    end
                elseif properties:IsPositioningInY()
                    number targetY = parentHeight * properties:GetPercentageY() + properties:GetPixelY() - height * properties:GetPercentageOriginY()

                    if properties:IsSnappingToPixels()
                        targetY = cast(integer, targetY)
                    end

                    container:SetY(targetY)
                end

                if properties:IsSettingWidth()
                    if properties:IsSettingHeight()
                        container:SetSize(width, height)
                    else
                        container:SetWidth(width)
                    end
                elseif properties:IsSettingHeight()
                    container:SetHeight(height)
                end
            end
        end

        integer counter = 0

        repeat while counter < items:GetSize()
            Item2D item = items:Get(counter)
            if item is Control
                Control control = cast(Control, item)
                control:ResetLayoutFlag()
                LayoutProperties properties = control:GetLayoutProperties(containerWidth, containerHeight)

                if properties not= undefined
                    if properties:NeedsRendering()
                        control:LoadGraphics(properties)
                    end

                    number width = control:GetWidth()
                    number height = control:GetHeight()

                    if properties:IsSettingWidth()
                        width = containerWidth * properties:GetPercentageWidth() + properties:GetPixelWidth() * properties:GetInterfaceScale()
                    end

                    if properties:IsSettingHeight()
                        height = containerHeight * properties:GetPercentageHeight() + properties:GetPixelHeight() * properties:GetInterfaceScale()
                    end

                    if width < properties:GetMinimumWidth()
                        width = properties:GetMinimumWidth()
                    elseif width > properties:GetMaximumWidth()
                        width = properties:GetMaximumWidth()
                    end

                    if height < properties:GetMinimumHeight()
                        height = properties:GetMinimumHeight()
                    elseif height > properties:GetMaximumHeight()
                        height = properties:GetMaximumHeight()
                    end

                    if properties:IsSnappingToPixels()
                        width = cast(integer, width)
                        height = cast(integer, height)
                    end

                    if properties:IsPositioningInX()
                        if properties:IsPositioningInY()
                            number targetX = containerWidth * properties:GetPercentageX() + properties:GetPixelX() - width * properties:GetPercentageOriginX()
                            number targetY = containerHeight * properties:GetPercentageY() + properties:GetPixelY() - height * properties:GetPercentageOriginY()

                            if properties:IsSnappingToPixels()
                                targetX = cast(integer, targetX)
                                targetY = cast(integer, targetY)
                            end

                            control:SetPosition(targetX, targetY)
                        else
                            number targetX = containerWidth * properties:GetPercentageX() + properties:GetPixelX() - width * properties:GetPercentageOriginX()

                            if properties:IsSnappingToPixels()
                                targetX = cast(integer, targetX)
                            end

                            control:SetX(targetX)
                        end
                    elseif properties:IsPositioningInY()
                        number targetY = containerHeight * properties:GetPercentageY() + properties:GetPixelY() - height * properties:GetPercentageOriginY() 

                        if properties:IsSnappingToPixels()
                            targetY = cast(integer, targetY)
                        end

                        control:SetY(targetY)
                    end

                    if properties:IsSettingWidth()
                        if properties:IsSettingHeight()
                            control:SetSize(width, height)
                        else
                            control:SetWidth(width)
                        end
                    elseif properties:IsSettingHeight()
                        control:SetHeight(height)
                    else
                        /*
                        Setting the size of a control triggers its Resize. Even
                        if we're not changing its size, we need to ensure that
                        the Resize propagates all the way through the children
                        hierarchy.
                        */
                        control:Resize()
                    end
                end
            end

            counter = counter + 1
        end
    end

end