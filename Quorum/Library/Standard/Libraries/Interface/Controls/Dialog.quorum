package Libraries.Interface.Controls

use Libraries.Game.Game
use Libraries.Game.GameStateManager
use Libraries.Game.Layer
use Libraries.Game.DialogLayer
use Libraries.Game.Graphics.Color
use Libraries.Game.Graphics.Gradient
use Libraries.Game.Graphics.Font
use Libraries.Game.Graphics.Label
use Libraries.Game.Graphics.Texture
use Libraries.Game.Graphics.PixelMap
use Libraries.Game.Graphics.Format
use Libraries.Interface.Behaviors.Controls.DialogHideBehavior
use Libraries.Interface.Layouts.LayoutProperties
use Libraries.Interface.Layouts.FlowLayout
use Libraries.Interface.Layouts.ManualLayout
use Libraries.Interface.Views.LabelBoxView
use Libraries.Interface.Behaviors.Behavior

class Dialog is Control
    Behavior defaultBehavior = undefined
    
    on create
        SetName("Dialog")
        SetInputGroup("Dialog")
        
        LayoutProperties properties = GetDefaultLayoutProperties()
        properties:SetHorizontalLayoutMode(properties:STANDARD)
        properties:SetVerticalLayoutMode(properties:FIT_CONTENTS)
        properties:SetPercentageX(0.5)
        properties:SetPercentageY(0.5)
        properties:SetPercentageOriginX(0.5)
        properties:SetPercentageOriginY(0.5)
        SetAccessibilityCode(parent:Item:DIALOG)
        parent:Item:visible = false
    end

    GameStateManager manager
    boolean isModal = false
    boolean showTopBar = true

    Control topBar

    /*
        This action gets a default behavior for a hypothetical "ok" style button
        that the dialog will fire if "enter" is pressed on the keyboard. If no behavior
        is set, then no work will be done.
    */
    action GetBehavior returns Behavior
        return defaultBehavior
    end

    /*
        This action sets a default behavior for a hypothetical "ok" style button
        that the dialog will fire if "enter" is pressed on the keyboard. If no behavior
        is set, then no work will be done.
    */
    action SetBehavior(Behavior behavior)
        defaultBehavior = behavior
    end

    /*
    This action displays the Dialog in the Game window. When the Dialog is
    displayed, the OnShow() action will be called.
    */
    action Show
        if IsShowing()
            return now
        end
        Game game = manager:GetGame()
        DialogLayer layer = game:GetDialogLayerPool():GetDialogLayer()

        layer:SetModal(isModal)
        game:AddLayer(layer)

        FlowLayout layout
        SetLayout(layout)

        ManualLayout layerLayout
        layer:SetLayout(layerLayout)

        LayoutProperties properties = GetDefaultLayoutProperties()

        if showTopBar and GetChildren():Get(0):Equals(topBar) = false
            Control bar = topBar
            LabelBoxView barView

            Color color
            Gradient gradient
            Color gray = color:CustomColor(0.82, 0.82, 0.82, 1)
            Color lightGray = color:CustomColor(0.93, 0.93, 0.93, 1)
            gradient:Set(gray, gray, lightGray, lightGray)
            
            barView:Initialize(gradient, color:Black())
            barView:SetBorderThickness(1)
            // Top, bottom, left, and right borders
            barView:SetBorderStyle(barView:ALL)
            bar:SetView2D(barView)

            Label nameLabel
            Font font = properties:GetFont()
            if font = undefined
                font = nameLabel:GetFont()
            end

            LayoutProperties barProperties = bar:GetDefaultLayoutProperties()
            // Bar will use manual layout for now, which doesn't respect these.
//            barProperties:SetHorizontalLayoutMode(barProperties:STANDARD)
//            barProperties:SetVerticalLayoutMode(barProperties:FIT_FONT)
            barProperties:SetFont(font)
            barProperties:SetPercentageWidth(1.0)
//            barProperties:SetPixelWidth(200)
            barProperties:SetPixelHeight(20)

            ManualLayout manualLayout
            bar:SetLayout(manualLayout)

            LayoutProperties labelProperties = nameLabel:GetDefaultLayoutProperties()
            labelProperties:SetPercentageOriginX(0.5)
            labelProperties:SetPercentageX(0.5)
            labelProperties:SetPercentageOriginY(0.5)
            labelProperties:SetPercentageY(0.5)
            labelProperties:SetPixelY(4)

            nameLabel:SetText(GetName())
            bar:Add(nameLabel)

            Button closeButton
            DialogHideBehavior hideBehavior
            hideBehavior:SetDialog(me)
            closeButton:SetBehavior(hideBehavior)

            Icon closeIcon
            Color white = color:White()
            Texture theX = GenerateX(20, 4, 2, white)
            closeIcon:Load(theX)
            closeIcon:SetColor(white)
            closeButton:SetIcon(closeIcon)

            LayoutProperties buttonProperties = closeButton:GetDefaultLayoutProperties()
            buttonProperties:SetHorizontalLayoutMode(buttonProperties:STANDARD)
            buttonProperties:SetVerticalLayoutMode(buttonProperties:STANDARD)
            buttonProperties:SetPixelHeight(20)
            buttonProperties:SetPixelWidth(20)
            buttonProperties:SetPercentageOriginX(1.0)
            buttonProperties:SetPercentageX(1)
            buttonProperties:SetPercentageOriginY(1.0)
            buttonProperties:SetPercentageY(1)
            buttonProperties:SetIconColor(white)
            buttonProperties:SetFont(undefined)

            closeButton:SetName("Close")

            bar:Add(closeButton)

            Add(0, bar)
        end

        layer:SetInterfaceScale(game:GetInterfaceScale())

        layer:OnShow()
        layer:Add(me)

        parent:Item:Show()
        OnShow()
    end

    /*
    This action generates the texture used for the close button in the Dialog.
    */
    private action GenerateX(integer size, integer buffer, integer width, Color color) returns Texture
        PixelMap map
        Format format
        format:SetValue(format:RGBA8888)
        map:CreatePixelMap(size, size, format)
        
        Color background
        background:SetColor(237.0 / 255.0, 113.0 / 255.0, 107.0 / 255.0, 1.0)

        map:FillRectangle(1, 1, size - 2, size - 2, background)
        map:DrawRectangle(0, 0, size, size, background)

        integer x = buffer
        integer y = buffer
        repeat while x < size - buffer
            map:SetPixel(x, y, color)
            map:SetPixel(x, size - y - 1, color)

            integer theWidth = 1
            repeat while theWidth < width
                if y + theWidth < size - buffer
                    map:SetPixel(x, y + theWidth, color)
                end

                if x + theWidth < size - buffer
                    map:SetPixel(x + theWidth, y, color)
                end
                theWidth = theWidth + 1
            end

            theWidth = 1
            repeat while theWidth < width
                if x + theWidth < size - buffer
                    map:SetPixel(x + theWidth, size - y - 1, color)
                end

                if size  - buffer- y - theWidth - 1 >= 0
                    map:SetPixel(x, size - y - theWidth - 1, color)
                end
                theWidth = theWidth + 1
            end

            x = x + 1
            y = y + 1
        end

        Texture texture
        texture:LoadFromPixelMap(map)
        return texture
    end

    /*
    This action is called whenever the Dialog is displayed on the screen. This
    action does nothing by default, but can be overriden with user-defined code.
    */
    action OnShow
        
    end

    /*
    This action closes the Dialog, if it's currently displayed in the Game. When
    the Dialog is hidden with this call, the OnHide() action will be called.
    */
    action Hide
        Game game = manager:GetGame()
        Layer layer = GetLayer()
        if layer is DialogLayer
            DialogLayer dialogLayer = cast(DialogLayer, layer)
            dialogLayer:Remove(me)
            dialogLayer:OnHide()
            game:RemoveLayer(dialogLayer)

            game:GetDialogLayerPool():Recycle(dialogLayer)
        else
            return now
        end
        
        parent:Item:Hide()
        OnHide()
    end

    /*
    This action is called when the Dialog is hidden with the Hide() action. By
    default this action does nothing, but it can be overriden with user-defined
    code to perform tasks when the Dialog is closed.
    */
    action OnHide

    end

    /*
    This action is typically used to set the parent of an Item2D or Control, but
    Dialogs can't be added as children of other items directly. Calling this
    action with any value that isn't undefined will alert a runtime error.
    */
    action SetParent(Item2D item)
        if item not= undefined
            alert("I couldn't set the item " + item:GetName() + " as the parent of the dialog " + GetName() + " because Dialogs can't have parent Items.")
        else
            parent:Control:SetParent(item)
        end
    end

    /*
    This action is used to set if the Dialog is modal or not. A modal Dialog 
    prevents input from reaching other parts of the application while the
    Dialog is displayed -- for example, clicking outside of the Dialog on a
    Button will not trigger the Button if the Dialog is modal.
    */
    action SetModal(boolean value)
        isModal = value
    end

    /*
    This action is used to determine if the Dialog is modal or not. A modal Dialog 
    prevents input from reaching other parts of the application while the
    Dialog is displayed -- for example, clicking outside of the Dialog on a
    Button will not trigger the Button if the Dialog is modal.
    */
    action IsModal returns boolean
        return isModal
    end

    /*
    This action determines whether or not the top bar should be visible on this
    Dialog. This value must be set before the Dialog is shown -- if this is
    changed while the Dialog is open, it won't affect the Dialog until the next
    time it's opened.
    */
    action SetTopBarVisible(boolean enable)
        showTopBar = enable
    end

    action Resize
        Layer layer = GetLayer()
        if layer not= undefined
            layer:SetViewport(0, 0, manager:GetGameDisplay():GetWidth(), manager:GetGameDisplay():GetHeight())
            layer:GetCamera():Update()
        end
        parent:Control:Resize()
    end

    /*
    This action returns whether or not the top bar will be displayed as part of
    this Dialog when it opens.
    */
    action IsTopBarVisible returns boolean
        return showTopBar
    end

    action IsAccessibleParent returns boolean
        return true
    end
end